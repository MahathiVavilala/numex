# NumEx

We are undergrad students, enamored with Elixir.
We hope to build a library in Elixir-lang that has some of the features of NumPy.
______________
Contributors :   
Mahathi Vavilala: mahathivavilala97@gmail.com   
Sravya Nimmagadda : nimmagadda.sravya97@gmail.com

**TODO: Add description**

## Installation

 The package can be installed
by adding `num_ex` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:num_ex, "~> 0.1.0"}
  ]
end
```

The project documentation can be found at [https://hexdocs.pm/num_ex](https://hexdocs.pm/num_ex).

Project Documentation was generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm).
