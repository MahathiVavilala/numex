defmodule NumExTest do
  use ExUnit.Case
  doctest Sort
  test "sorts the elements in quicksort fashion" do
    l = [195,99,78,12]
    expected = [12,78,99,195]
    assert Sort.quicksort(l) == expected
  end
  test "performs lexographicsort" do
    l1 = [5, 3, 6, 8, 1]
    l2 = [0, 9, 4, 7, 2]
    expected = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    assert Sort.lexsort(l1, l2) == expected
  end

  test "Return a sorted copy of a List of Lists based on the value of axis" do
    l1 = [[6, 2], [5, 7], [0, 9]]
    axis = 1
    expected = [[2, 6], [5, 7], [0, 9]]
    assert Sort.sort(l1, axis) == expected
  end

  test "Return sorted enumberble along one axis only" do
    l1 = [[3, 4], [0, 1], [6, 5]]
    expected = [[0, 1], [3, 4], [5, 6]]
    assert Sort.msort(l1) == expected
  end

  doctest Count
  test "count all non-zero elements" do
    l1 = [1,0,9,0,3,0,5]
    expected = 4
    assert Count.count_nonzero(l1) == expected
  end

  doctest Decimal_
  test "Compare two Decimals" do
    d1 = 1
    d2 = 5
    expected = -1
    assert Decimal_.compare(d1, d2) == expected
  end
end
